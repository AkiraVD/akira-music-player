//Play bài hát

function playSong() {
  if (trackIndex == -1) {
    trackIndex = 0;
    loadSong(trackIndex);
  }
  setVolume();
  currentTrack.play();
  isPlaying = true;

  //Đổi icon
  document.querySelector("#music_player .btn-play").style.display = "none";
  document.querySelector("#music_player .btn-pause").style.display = "contents";
  document.querySelector("#music_player .thumbnail").style.animation =
    "spin 20s linear infinite";
}

function pauseSong() {
  currentTrack.pause();
  isPlaying = false;

  //Đổi icon
  document.querySelector("#music_player .btn-play").style.display = "contents";
  document.querySelector("#music_player .btn-pause").style.display = "none";
  document.querySelector("#music_player .thumbnail").style.animation = "none";
}

// Random between 2 value
function get2val(min, max) {
  return Math.random() * (max - min) + min;
}

function playRandomSong() {
  trackIndex = Math.floor(get2val(0, songList.length));
  loadSong(trackIndex);
  playSong();
  return;
}
function playNextSong() {
  trackIndex++;
  loadSong(trackIndex);
  playSong();
  return;
}
function playFirstSong() {
  trackIndex = 0;
  loadSong(trackIndex);
  playSong();
  return;
}

function nextSong() {
  switch (loopMode) {
    case "none":
      if (isRandom) {
        playRandomSong();
        return;
      } else {
        playNextSong();
        return;
      }
    case "loop":
      if (isRandom) {
        playRandomSong();
        return;
      } else if (trackIndex !== songList.length - 1) {
        playNextSong();
        return;
      } else {
        playFirstSong();
        return;
      }
    case "loop_one":
      loadSong(trackIndex);
      playSong();
      return;
  }
}
function nextBtn() {
  trackIndex++;
  console.log("trackIndex: ", trackIndex);
  if (trackIndex == songList.length) {
    trackIndex = 0;
  }
  loadSong(trackIndex);
  playSong();
}
function prevBtn() {
  // Quay về bài cuối nếu bài hiện tại là bài đầu
  if (trackIndex > 0) trackIndex -= 1;
  else trackIndex = songList.length - 1;

  loadSong(trackIndex);
  playSong();
}

var isRandom = false;
var clickRandom = 0;
function randomSong() {
  clickRandom++;
  switch (clickRandom % 2) {
    case 0:
      document
        .querySelector("#music_player .btn-random")
        .classList.remove("active");
      isRandom = false;
      return;
    case 1:
      document
        .querySelector("#music_player .btn-random")
        .classList.add("active");
      isRandom = true;
      return;
  }
}

var loopMode = "none";
var click = 0;
function loopSong() {
  click++;
  switch (click % 3) {
    case 0:
      document.querySelector("#music_player .btn-loop i").innerHTML = `repeat`;
      document
        .querySelector("#music_player .btn-loop")
        .classList.remove("active");
      loopMode = "none";
      break;
    case 1:
      document.querySelector("#music_player .btn-loop").classList.add("active");
      loopMode = "loop";
      break;
    case 2:
      document.querySelector(
        "#music_player .btn-loop i"
      ).innerHTML = `repeat_one`;
      loopMode = "loop_one";
      break;
  }
}
