//Lấy thông tin từ website
let nowPlaying = document.querySelector("#music_player .nowPlaying");
let thumbnail = document.querySelector("#music_player .thumbnail");
let songName = document.querySelector("#music_player .songName");
let artist = document.querySelector("#music_player .artist");

let playBtn = document.querySelector("#music_player .btn-play");
let backwardBtn = document.querySelector("#music_player .btn-backward");
let forwardBtn = document.querySelector("#music_player .btn-forward");
let randomBtn = document.querySelector("#music_player .btn-random");
let loopBtn = document.querySelector("#music_player .btn-loop");

let timeSlider = document.querySelector("#music_player .timeSlider");
let volumeBar = document.querySelector("#music_player .volumeBar");
let currentTime = document.querySelector("#music_player .currentTime");
let songDuration = document.querySelector("#music_player .songDuration");

// Các giá trị chung
let trackIndex = -1;
let isPlaying = false;
let updateTimer;

//Thay đổi các giá trị về ban đầu
function resetValue() {
  currentTime.textContent = "00:00";
  songDuration.textContent = "00:00";
  timeSlider.value = 0;
}

// Tạo element audio cho music player
let currentTrack = document.getElementById("music-player-audio");

function seekTo() {
  //Tính toán vị trí trên thanh thời gian
  let time = currentTrack.duration * (timeSlider.value / 100);

  //Chỉnh vị trí hiện tại về vị trí đã tính toán
  currentTrack.currentTime = time;
}

function setVolume() {
  //Chỉnh volume dựa theo bar
  currentTrack.volume = volumeBar.value / 100;
  if (volumeBar.value > 70) {
    document.querySelector("#music_player .volumeIcon i").innerHTML =
      "volume_up";
  } else if (volumeBar.value > 30) {
    document.querySelector("#music_player .volumeIcon i").innerHTML =
      "volume_down";
  } else if (volumeBar.value > 0) {
    document.querySelector("#music_player .volumeIcon i").innerHTML =
      "volume_mute";
  } else if ((volumeBar.value = 0)) {
    document.querySelector("#music_player .volumeIcon i").innerHTML =
      "volume_off";
  }
}

function seekUpdate() {
  let seekPosition = 0;

  // Kiểm tra độ dài có phải số nguyên không
  if (!isNaN(currentTrack.duration)) {
    seekPosition = currentTrack.currentTime * (100 / currentTrack.duration);
    timeSlider.value = seekPosition;

    // Kiểm tra thời gian còn lại và tổng thời gian
    let currentMinutes = Math.floor(currentTrack.currentTime / 60);
    let currentSeconds = Math.floor(
      currentTrack.currentTime - currentMinutes * 60
    );
    let durationMinutes = Math.floor(currentTrack.duration / 60);
    let durationSeconds = Math.floor(
      currentTrack.duration - durationMinutes * 60
    );

    // Thêm số 0 vào đơn vị chỉ có 1 chữ số
    if (currentSeconds < 10) {
      currentSeconds = "0" + currentSeconds;
    }
    if (durationSeconds < 10) {
      durationSeconds = "0" + durationSeconds;
    }
    if (currentMinutes < 10) {
      currentMinutes = "0" + currentMinutes;
    }
    if (durationMinutes < 10) {
      durationMinutes = "0" + durationMinutes;
    }

    // Hiển thị và cập nhật thời gian
    currentTime.textContent = currentMinutes + ":" + currentSeconds;
    songDuration.textContent = durationMinutes + ":" + durationSeconds;
  }
}

// function getTextWidth(songName) {
//   return Math.ceil(songName.clientWidth);
// }

// Load bài hát từ danh sách
function loadSong(trackIndex) {
  //Clear thời gian đang chọn cũ
  clearInterval(updateTimer);
  resetValue();

  //Load Bài mới
  currentTrack.volume = 0.2;
  currentTrack.src = songList[trackIndex].src;
  currentTrack.load();

  //Update Thông tin bài hát
  thumbnail.src = songList[trackIndex].img;
  songName.textContent = songList[trackIndex].name;
  artist.textContent = songList[trackIndex].artist;
  nowPlaying.textContent = `Playing song ${trackIndex + 1} of ${
    songList.length
  }`;

  //Scrolling khi tên bài hát quá dài
  var n = 40;
  if (songName.innerHTML.length > 24) {
    document.querySelector(
      "#music_player .songName"
    ).style.animation = `scroll_text ${n * 12}s linear infinite`;
    let text = " ";
    for (var i = 0; i < n; i++) {
      text += ` ${songName.textContent} &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; `;
    }
    songName.innerHTML = text;
  } else {
    document.querySelector("#music_player .songName").style.animation = "none";
  }

  // Chỉnh thời gian cập nhật timer
  updateTimer = setInterval(seekUpdate, 1000);

  // Tự động chỉnh qua bài mới khi kết thúc bài cũ
  currentTrack.addEventListener("ended", nextSong);
}
