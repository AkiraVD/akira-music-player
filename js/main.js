//Song lists
let songList = [];

const DATA_URL = "https://63676a8b79b0914b75dff07d.mockapi.io/songlist";

// Lấy thông tin từ database
function getData() {
  axios({
    url: DATA_URL,
    method: "GET",
  })
    .then(function (res) {
      res.data.forEach((item) => {
        songList.push(item);
        songListRender();
      });
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

getData();

// Song Model
class Song {
  constructor(name, artist, src, img) {
    this.name = name;
    this.artist = artist;
    this.src = src;
    this.img = img;
  }
}

function addSong() {
  var name = document.getElementById("songName").value;
  var artist = document.getElementById("songArtist").value;
  var src = document.getElementById("songURL").value;
  var img = document.getElementById("imgURL").value;

  let data = { name, artist, src, img };
  songList = [];
  axios({
    url: DATA_URL,
    method: "POST",
    data: data,
  })
    .then(function (res) {
      getData();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function previewAlbum() {
  var imgURL = document.getElementById("imgURL").value;
  document.querySelector("#imgPreview img").src = imgURL;
}

function songListToggle(action) {
  if (action == "open") {
    document.querySelector("#music_player .container .songList").style.display =
      "block";
    document.querySelector(
      "#music_player .container .songListClose"
    ).style.display = "block";
    document.querySelector(
      "#music_player .container .songListToggle"
    ).style.display = "none";
  } else if (action == "close") {
    document.querySelector("#music_player .container .songList").style.display =
      "none";
    document.querySelector(
      "#music_player .container .songListClose"
    ).style.display = "none";
    document.querySelector(
      "#music_player .container .songListToggle"
    ).style.display = "block";
  }
}

function songListRender() {
  let contentHTML = "";
  songList.forEach((item, index) => {
    contentHTML += `
    <tr>
      <td class="text-start" onclick="loadSong(${index});playSong()">${item.name} - ${item.artist}</td>
    </tr>
    `;
  });
  document.getElementById("songListTable").innerHTML = contentHTML;
}
